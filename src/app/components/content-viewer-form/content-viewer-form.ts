import { Component } from '@angular/core';
import {
    FormGroup,
    AbstractControl,
    Validators,
    FormBuilder,
} from '@angular/forms';
import * as CustomValidators from './custom-validators'

import { DummyContentService } from '../../services/dummy-content/dummy-content';

import { Content } from '../../models/content';

@Component({
    moduleId: module.id,
    selector: 'content-viewer-form',
    templateUrl: 'content-viewer-form.html'
})

export class ContentViewerFormComponent {

    myForm: FormGroup;

    /*debug*/
    stringify: any = JSON.stringify;

    collectionControl: any = {};
    configMessages: any = {};

    rows: Content[] = [{
        "name":"",
        "email":"",
        "phone":"",
        "reference":"",
        "concept":"",
        "import":""
    }];


    constructor(
        private _dummyContentService: DummyContentService,
        private formBuilder: FormBuilder
    ) {
        this.formBuilder = formBuilder;
        this.buildValidation();

        this._dummyContentService.getData().subscribe((content) => {
            this.rows = content.data;
            this.buildValidation();
        });
    }

    buildValidation(){
        let configValidation = {};
        this.rows.map((row, index) => {
            configValidation[index] = this.formBuilder.group({
                "name": [row.name, [
                    Validators.required,
                    CustomValidators.validateName
                ]],
                "email": [row.email, [
                    Validators.required,
                    CustomValidators.validateEmail
                ]],
                "phone": [row.phone, [
                    CustomValidators.validatorPhone
                ]],
                "reference": [row.reference, [
                    Validators.required
                ]],
                "concept": [row.concept, [
                    Validators.required
                ]],
                "import": [row.import, [
                    Validators.required
                ]]
            });
        })

        this.configMessages = {
            name: {
                required: "Campo requerido",
                name: "No es un nombre válido"
            },
            email: {
                required   : 'Campo requerido',
                email      : 'Error de formato de email.'
            },
            phone: {
                phone: 'Formato incorrecto'
            },
            reference: {
                required   : 'Campo requerido'
            },
            concept: {
                required   : 'Campo requerido'
            },
            "import": {
                required   : 'Campo requerido'
            }
        };

        this.myForm = this.formBuilder.group(configValidation);

        this.rows.map((row, index) => {
            this.collectionControl[index] = {};
            this.collectionControl[index]["name"] = this.myForm.get([index, "name"]);
            this.collectionControl[index]["email"] = this.myForm.get([index, "email"]);
            this.collectionControl[index]["phone"] = this.myForm.get([index, "phone"]);
            this.collectionControl[index]["reference"] = this.myForm.get([index, "reference"]);
            this.collectionControl[index]["concept"] = this.myForm.get([index, "concept"]);
            this.collectionControl[index]["import"] = this.myForm.get([index, "import"]);
        })
    }

    generateNewRowsFromMyForm(formData: any, indexRow?: number){
        let newRows: any[] = [];
        let ignoreRow: boolean = false;
        if(indexRow){
            ignoreRow = true;
        }
        for(let row in formData){
            if(ignoreRow ){
                if( indexRow != parseInt( row ) ){
                    newRows.push(formData[row]);
                }
            } else{
                newRows.push(formData[row]);
            }
        }
        return newRows;
    }

    deleteContent(index: number){
        //let newRows = this.rows.slice(0);
        //newRows = newRows.filter((row, indexRow) => {
        //    return indexRow != index
        //});
        console.log("myForm", this.myForm);
        this.rows = this.generateNewRowsFromMyForm(this.myForm.value, index);
        this.buildValidation();

    }

}
