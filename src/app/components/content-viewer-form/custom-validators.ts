import { FormControl } from '@angular/forms';

export function validateName(c: FormControl) {
    let NAME_REGEXP = /[a-zA-Z]+/g;
    return NAME_REGEXP.test(c.value) ? null: {
        name: {
            valid: false
        }
    };
}

export function validateEmail(c: FormControl){
    let EMAIL_REGEXP = /^[a-z0-9\_\-\.]+[a-z0-9\.][a-z0-9\_\-\.]+@\w+([\.-]?\w+)(\.[a-z]{2,10})$/;
    return EMAIL_REGEXP.test(c.value) ? null: {
        email: {
            valid: false
        }
    };
}

export function validateNumber(c: FormControl){
    let NUMBER_REGEXP = /[0-9]/;
    return NUMBER_REGEXP.test(c.value) ? null: {
        number: {
            valid: false
        }
    };
}

export function validatorPhone(c: FormControl){
    let REGEX_PHONE = /^(\+){0,1}(\d|\s|\(|\)){7,10}$/;
    let REGEX_CELLPHONE = /^(\#[0-9]{6}|\*[0-9]{6}|[0-9]{7}|\#?[0-9]{9}|[0-9]{2}\*[0-9]{3}\*[0-9]{4})$/gi;

    let conditionPhone: boolean = REGEX_PHONE.test(c.value);
    let conditionCellPhone: boolean = REGEX_CELLPHONE.test(c.value);
    let result = conditionPhone || conditionCellPhone || c.value === "";

    return result ? null: {
        phone: {
            valid: false
        }
    };
}
