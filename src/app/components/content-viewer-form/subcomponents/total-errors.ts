import { Component, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'total-errors',
    template: `<div class="total-errors">
        <span class="total-errors__item"> Total de errores encontrados: {{  totalErrors  }}</span>
               </div>`,
})

export class ContentViewerFormTotalErrorsComponent {
    @Input() form: any;
    totalErrors: number = 0;

    constructor(){
    }


    ngDoCheck(){
        if(this.form){
            this.totalErrors = this.getTotalErrors(this.form);
        }
    }

    getTotalErrors(form: any){
        let _total: number = 0;
        let formGroups = form.controls;

        function readControlKeys(controls: any){
            for(let controlName in controls){
                if(controls[controlName].errors){
                    _total++;
                }
            }
        }

        for(let nameFormGroup in formGroups){
            let controlsOfFormGroup = formGroups[nameFormGroup].controls;
            readControlKeys(controlsOfFormGroup);
        }

        return _total;
    }
}
