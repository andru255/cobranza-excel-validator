import { Component, Input, OnChanges } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: "validator-messages",
    templateUrl: "validator-messages.html"
})

export class ValidatorMessagesComponent implements OnChanges {
    errorMessage: string = '';
    isErrors: boolean;

    @Input()
    errors: any;

    @Input()
    options: any;

    toggleShowErrors(options: any) {
        if (options.element.valid || (options.element.pristine && !options.submitted)) {
            this.isErrors = false;
        } else {
            this.isErrors = true;
        }
    }

    ngOnChanges(changes: any): void {
        let errors: any = changes.options.currentValue.element.errors;
        this.errorMessage = '';
        this.toggleShowErrors(changes.options.currentValue);

        if (errors) {
            Object.keys(this.options.messages).some(key => {
                if (errors[key]) {
                    this.errorMessage = this.options.messages[key];
                    return true;
                }
            });
        }
    }
}
