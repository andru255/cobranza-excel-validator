import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class DummyContentService {
    private _content: any;

    constructor(private _http: Http){
    }

    getData(){
        return this._http.get('./data/data.json')
            .map( res => this._content = res.json());
    }
}
