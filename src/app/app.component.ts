import { Component } from '@angular/core';
import { DummyContentService } from './services/dummy-content/dummy-content';

@Component({
    selector: 'my-app',
    template: `<content-viewer-form></content-viewer-form>`,
    providers:[ DummyContentService ]
})

export class AppComponent  { name = 'Angular'; }
