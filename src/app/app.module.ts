import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';

/* inicio son lo unico a usar*/
import { ContentViewerFormTotalErrorsComponent } from './components/content-viewer-form/subcomponents/total-errors';
import { ValidatorMessagesComponent } from './components/content-viewer-form/validator/validator-messages';
import { ContentViewerFormComponent } from './components/content-viewer-form/content-viewer-form';
/* fin son lo unico a usar*/

import { AppComponent } from './app.component';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        ReactiveFormsModule
    ],
    declarations: [
        ContentViewerFormTotalErrorsComponent,
        ValidatorMessagesComponent,
        ContentViewerFormComponent,
        AppComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
