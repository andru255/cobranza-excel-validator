export class Content {
    name: string;
    email: string;
    phone: string;
    reference: string;
    concept: string;
    "import": string;
}
